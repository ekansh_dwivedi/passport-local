const express = require('express');
const router = express.Router();
const path = require('path');
const passport = require('passport');

const User = require('../models/user');

router.get('/',function(req,res){
    res.render('index');
});


router.get('/login',function(req,res){
    res.render('login');
});


router.get('/register',function(req,res){
    res.render('register');
});

router.post('/register',function(req,res){
    var name = req.body.name;
	var email = req.body.email;
	var username = req.body.username;
	var password = req.body.password;
	
    var newUser = new User({
        name: name,
        email:email,
        username: username,
        password: password
    });

    User.createUser(newUser, function(err, user){
        if(err) throw err;
    });
    
   res.render("login");
});


router.get('/logout', function(req, res){
    req.logout();
	res.redirect('/');
});

router.get('/login', function(req, res){
    res.render('login');
});


router.get('/dashboard',function(req, res){
    res.render('dashboard');
});


module.exports = router;