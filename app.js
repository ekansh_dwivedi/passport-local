const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');
const expressValidator = require('express-validator');
const flash = require('connect-flash');
const session = require('express-session');
const cookieSession = require('cookie-session');
const route = require('./routes/app-route');
const mongo = require('mongodb');
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/loginapp');
var db = mongoose.connection;

const app = express();

app.use(flash());


app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

const port = 3000;

app.use(cookieSession({
    maxAge: 24 * 60 * 60 * 1000,
    keys: ['ekansh']
}));

// View Engine
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', exphbs({defaultLayout:'layout'}));
app.set('view engine', 'handlebars');

//variables for flash msgs
app.use(function (req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
});
  

app.use(express.static(path.join(__dirname, 'public')));


app.use('/',route);

app.listen(port , function(err){
    if(err){
        throw err;
    }else{
        console.log("server started");    
    }
});

